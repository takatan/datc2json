use std::fs;
use sxd_document::parser;
use sxd_xpath::{evaluate_xpath, Value, Factory, Context};
use std::collections::HashMap;


pub fn read_xml() -> HashMap<String, String> {
    let xml = fs::read_to_string("resources/std_adjacency.xml").unwrap();
    let root = parser::parse(xml.as_ref()).unwrap();
    let doc = root.as_document();
    let value = evaluate_xpath(&doc, "/PROVINCES/PROVINCE").unwrap();

    let factory = Factory::new();
    let mut map = HashMap::new();

    if let Value::Nodeset(nodes) = value {
        for node in nodes.document_order() {
            let shortname = node.element().unwrap().attribute_value("shortname").unwrap();
            map.insert(shortname.to_string(), shortname.to_string());
            let fullname = node.element().unwrap().attribute_value("fullname").unwrap();
            map.insert(fullname.to_string(), shortname.to_string());

            let xpath = factory.build(".//UNIQUENAME").unwrap();
            let xpath = xpath.unwrap();

            let context = Context::new();

            let names = xpath.evaluate(&context, node).unwrap();

            if let Value::Nodeset(ns) = names {
                for name in ns.document_order() {
                    let unique_name = name.element().unwrap().attribute_value("name").unwrap();
                    map.insert(unique_name.to_string(), shortname.to_string());
                }
            }
        }
    }
    return map;
}

#[cfg(test)]
mod test {
    use crate::provincedb::read_xml;

    #[test]
    fn test_read_xml() {
        read_xml();
    }
}